'use strict';

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'

export default class Key extends Component {
  constructor(props) {
    super(props);
    this.keyPressed = this.keyPressed.bind(this);
  }

  keyPressed(event){
    let key = event.target.innerHTML;
    console.log(key);
    this.props.dispatch({
      "type": "KEY_PRESSED",
      "key": key
    });
  }

  render() {
    return (
      <button onClick={this.keyPressed} className="key">{this.props.text}</button>
    )
  }
}
