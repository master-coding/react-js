'use strict';

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'

export default class Display extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <input id="display" type="text" value={this.props.value}/>
      </div>
    )
  }
}
