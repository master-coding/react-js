import { welcome } from './reducers/welcome'
import { messages } from './reducers/messages'

import { combineReducers } from 'redux'

const appStore = combineReducers({
  welcome: welcome,
  messages: messages,
})

export default appStore
