export function messages(state = [], action = {}) {
    switch (action.type) {
        case 'FIRST_BUTTON_MESSAGES':
            return ["we", "are", "learning", "js"];
        case 'SECOND_BUTTON_MESSAGES':
            return ["css", "js", "html"];
        default:
            return state;
    }
}
