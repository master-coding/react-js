export function welcome(state = "", action = {}) {
    switch (action.type) {
        case 'FIRST_BUTTON':
            return "Learning React JS is fun";
        case 'SECOND_BUTTON':
            return "Learning React JS is awesome";
        default:
            return state;
    }
}