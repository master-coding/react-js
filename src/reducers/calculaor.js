export function calculator(state = '', action = {}) {
  switch (action.type) {
    case 'KEY_PRESSED':
      return action.key
    default:
      return state
  }
}
